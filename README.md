# Document Image Segmentation Scoring

## A tool to score document image segmentation algorithms

This tool scores document image segmentation algorithms at pixel and object levels:
  - Pixel level:
    - Intersection-over-Union
    - Precision
    - Recall
    - F-score
  - Object level:
    - Average Precision (AP) at 50\%, 75\%, 95\%
    - Mean Average Precision (mAP) over the range 50-95\%


### Data format

This tool uses json files with the following format:
```
{
    "img_size": [
        height,
        width
    ],
    "class1": [
        {
            "confidence": confidence,
            "polygon": [...]
        }
    ],
    "class2": [
        {
            "confidence": confidence,
            "polygon": [...]
        },
        {
            "confidence": confidence,
            "polygon": [...]
        }
    ]
}
```
See the `demo` folder for an example of ground truth and prediction files.

Where `confidence` = mean confidence score of the predicted polygon. For the ground truth files: `"confidence": 1.0` for all the polygons. For the prediction files: `0.0 ≤ confidence ≤ 1.0` if known, otherwise `"confidence": 1.0`.


### Cloning and installation

```
git clone git@gitlab.com:teklia/dla/document_image_segmentation_scoring.git
mkvirtualenv evaluator -a ~/path/to/document_image_segmentation_scoring
pip install -e .
```


### Usage

To score a model, one should first generate the ground truth and prediction files according to the previous format. The ground truth and prediction files of an image should have the same names and be in different folders. Once the data is accordingly prepared:
```
run-evaluation path/to/labels path/to/predictions
```

To run the demo:
```
run-evaluation demo/labels demo/predictions
```


### Pixel level metrics

- Intersection-over-Union:
  - `IoU = sum(intersection areas) / (sum(gt areas) + sum(pred areas) - sum(intersection areas))`
  - If no object to detect on an image and no predicted object: `IoU = 1`
- Precision:
  - `P = sum(intersection areas) / sum(pred areas)`
  - If no predicted object on an image: `P = 1`
- Recall:
  - `R = sum(intersection areas) / sum(gt areas)`
  - If no object to detect on an image: `R = 1`
- F-score:
  - `F = 2*P*R / (P+R)`


### Object level metrics

- Average Precision:
  - Precision:
    - `P = nb_true_positive_objects / nb_pred_objects`
    - If no predicted objects: `P = 1`
  - Recall:
    - `R = nb_true_positive_objects / nb_gt_objects`
    - If no object to detect: `R = 1`
  - Precision-Recall curve interpolation
  - `AP = area under interpolated precision-recall curve`
- `AP@[.5]`: AP with an IoU threshold of 50\% to detect the true positive objects
- `AP@[.75]`: AP with an IoU threshold of 75\% to detect the true positive objects
- `AP@[.95]`: AP with an IoU threshold of 95\% to detect the true positive objects
- `AP@[.5,.95]`: mean AP over the range of IoU thresholds 50\% to 95\%


## Citing us

```latex
@inproceedings{boillet2022,
    author = {Boillet, Mélodie and Kermorvant, Christopher and Paquet, Thierry},
    title = {{Robust text line detection in historical documents: learning and evaluation methods}},
    booktitle = {International Journal on Document Analysis and Recognition (IJDAR)},
    year = {2022},
    month = Mar,
    pages = {1433-2825},
    doi = {10.1007/s10032-022-00395-7},
    url = {https://doi.org/10.1007/s10032-022-00395-7}
}
```
