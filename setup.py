#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from setuptools import setup


def parse_requirements():
    path = Path(__file__).parent.resolve() / "requirements.txt"
    assert path.exists(), f"Missing requirements: {path}"
    return list(map(str.strip, path.read_text().splitlines()))


setup(
    name="DocumentImageSegmentationEvaluator",
    version=open("VERSION").read(),
    description="Tool to score document image segmentation methods",
    author="Mélodie Boillet",
    author_email="boillet@teklia.com",
    install_requires=parse_requirements(),
    entry_points={"console_scripts": ["run-evaluation=evaluator.evaluate:main"]},
    packages=["evaluator", "tests"],
    long_description=open("README.md").read(),
)
