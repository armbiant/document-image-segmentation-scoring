#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The pixel metrics module
    ======================

    Available metrics:
        - Intersection-over-Union
        - Precision
        - Recall
        - F-score
"""

import numpy as np


def update_metrics(
    labels: list, predictions: list, classes: list, global_metrics: dict
) -> dict:
    """
    Compute the pixel level metrics between prediction and label areas of
    a given page and update the global set metrics.
    :param labels: The label's polygons.
    :param predictions: The predicted polygons.
    :param classes: The classes names.
    :param global_metrics: The initialized results dictionary.
    :return global_metrics: The updated results dictionary.
    """
    for channel in classes:
        # Compute intersection area (True Positive pixels) of the current class.
        intersection_area = 0
        for _, gt in labels[channel]:
            for _, pred in predictions[channel]:
                intersection_area += gt.intersection(pred).area
        # Compute ground truth and predicted areas for the current class.
        gt_area = np.sum([gt.area for _, gt in labels[channel]])
        pred_area = np.sum([pred.area for _, pred in predictions[channel]])

        # Add the page Intersection-over-Union value for the current class.
        global_metrics[channel]["iou"].append(
            get_iou(intersection_area, gt_area, pred_area)
        )

        # Add the page precision and recall for the current class.
        precision = get_precision(intersection_area, pred_area)
        recall = get_recall(intersection_area, gt_area)
        global_metrics[channel]["precision"].append(precision)
        global_metrics[channel]["recall"].append(recall)

        # Add the page f-score for the current class.
        if precision + recall != 0:
            global_metrics[channel]["fscore"].append(
                2 * precision * recall / (precision + recall)
            )
        else:
            global_metrics[channel]["fscore"].append(0)
    return global_metrics


def get_iou(intersection: float, label_area: float, predicted_area: float) -> float:
    """
    Get the Intersection-over-Union value between prediction and label areas of
    a given page.
    :param intersection: Area of the intersection.
    :param label_area: Area of the label objects.
    :param predicted_area: Area of the predicted objects.
    :return: The computed Intersection-over-Union value.
    """
    union = label_area + predicted_area - intersection
    # Nothing to detect and nothing predicted.
    if label_area == 0 and predicted_area == 0:
        return 1
    # Objects to detect and/or predicted but no intersection.
    if intersection == 0 and union != 0:
        return 0
    # Objects to detect and/or predicted that intersect.
    return intersection / union


def get_precision(intersection: float, predicted_area: float) -> float:
    """
    Get the precision between prediction and label areas of
    a given page.
    :param intersection: Area of the intersection.
    :param predicted_area: Area of the predicted objects.
    :return: The computed precision value.
    """
    # Nothing predicted.
    if predicted_area == 0:
        return 1
    return intersection / predicted_area


def get_recall(intersection, label_area) -> float:
    """
    Get the recall between prediction and label areas of
    a given page.
    :param intersection: Area of the intersection.
    :param label_area: Area of the label objects.
    :return: The computed recall value.
    """
    # Nothing to detect.
    if label_area == 0:
        return 1
    return intersection / label_area
