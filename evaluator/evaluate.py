#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The evaluation module
    ======================
"""

import argparse
import json
import logging
import os
import time

import numpy as np
from shapely.geometry import Polygon
from tqdm import tqdm

import evaluator.utils.object_metrics as o_metrics
import evaluator.utils.pixel_metrics as p_metrics

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def get_cli_args():
    """
    Get the command-line arguments.
    :return: The command-line arguments.
    """
    parser = argparse.ArgumentParser(description="Image segmentation evaluation tool")
    parser.add_argument(
        "labels_dir",
        type=str,
        help="Path to the labels directory.",
    )
    parser.add_argument(
        "predictions_dir",
        type=str,
        help="Path to the predictions directory.",
    )
    return parser.parse_args()


def read_json(filename: str) -> dict:
    """
    Read a label / prediction json file.
    :param filename: Path to the file to read.
    :return: A dictionary with the file content.
    """
    with open(filename, "r") as json_file:
        return json.load(json_file)


def retrieve_classes(labels_dir: str) -> list:
    """
    Retrieve the involved classes from the ground truth json files.
    :param labels_dir: Path to the labels directory.
    :return classes_names: List of found classes names.
    """
    classes_names = []
    for file_name in os.listdir(labels_dir):
        for element in read_json(os.path.join(labels_dir, file_name)).keys():
            if element not in classes_names:
                classes_names.append(element)
    classes_names.remove("img_size")
    return classes_names


def get_polygons(regions: dict, classes_names: list) -> dict:
    """
    Retrieve the polygons from a json file.
    :param regions: Regions extracted from a label / prediction file.
    :param classes_names: The classes names.
    :return: A dictionary with the retrieved polygons and the
             corresponding confidence scores.
    """
    return {
        channel: [
            (polygon["confidence"], Polygon(polygon["polygon"]).buffer(0))
            for polygon in regions[channel]
        ]
        for channel in classes_names
        if channel in regions.keys()
    }


def save_results(
    pixel_res: dict, pixel_metrics: dict, object_res: dict, classes_names: list
):
    """
    Print and save the results.
    :param pixel_res: The results obtained at pixel level.
    :param pixel_metrics: Names of the pixel level metrics.
    :param object_res: The results obtained at object level.
    :param classes_names: The names of the classes.
    """
    json_dict = {channel: {} for channel in classes_names}
    for channel in classes_names:
        print(channel)
        for metric in pixel_metrics:
            print(
                metric, " = ", np.round(np.mean(pixel_res[channel][metric.lower()]), 4)
            )
            json_dict[channel][metric] = np.round(
                np.mean(pixel_res[channel][metric.lower()]), 4
            )
        aps = object_res[channel]["ap"]
        print("AP [IOU=0.50] = ", np.round(aps[50], 4))
        print("AP [IOU=0.75] = ", np.round(aps[75], 4))
        print("AP [IOU=0.95] = ", np.round(aps[95], 4))
        print("AP [0.5,0.95] = ", np.round(np.mean(list(aps.values())), 4))
        json_dict[channel]["AP@[.5]"] = np.round(aps[50], 4)
        json_dict[channel]["AP@[.75]"] = np.round(aps[75], 4)
        json_dict[channel]["AP@[.95]"] = np.round(aps[95], 4)
        json_dict[channel]["AP@[.5,.95]"] = np.round(np.mean(list(aps.values())), 4)
        print("\n")

    with open("results.json", "w") as json_file:
        json.dump(json_dict, json_file, indent=4)


def run(labels_dir: str, pred_dir: str):
    """
    Run the evaluation.
    Retrieve the involved classes before starting evaluation.
    :param labels_dir: Path to the labels directory.
    :param pred_dir: Path to the predictions directory.
    """
    # Retrieve the classes.
    logging.info("Retrieving classes")
    classes_names = retrieve_classes(labels_dir)
    assert len(classes_names) > 0
    logging.info(
        f"Found {len(classes_names)} classes: {classes_names}"
        if len(classes_names) > 1
        else f"Found {len(classes_names)} class: {classes_names}"
    )

    # Initialize metrics and ranges.
    pixel_metrics = ["IoU", "Precision", "Recall", "Fscore"]
    object_metrics = ["Precision", "Recall", "Fscore", "AP"]
    ious_range = range(50, 100, 5)
    confidence_scores_range = range(95, -5, -5)

    logging.info("Starting evaluation")
    starting_time = time.time()

    # Results at pixel level: list of computed image values for each metric in
    # ['IoU', 'Precision', 'Recall', 'Fscore'] and for each channel in classes_names.
    pixel_res = {
        channel: {metric.lower(): [] for metric in pixel_metrics}
        for channel in classes_names
    }

    # Results at object level: dict of computed values for each IoU threshold in range(50, 100, 5),
    # metric in ['Precision', 'Recall', 'Fscore', 'AP'] and channel in classes_names.
    object_res = {
        channel: {metric.lower(): {} for metric in object_metrics}
        for channel in classes_names
    }
    # rank_scores contains the number of true positive objects and the total number of predicted objects
    # for each confidence score in range(95, -5, -5), IoU threshold in range(50, 100, 5)
    # and channel in classes_names.
    # Used while computing the precision at object level.
    rank_scores = {
        channel: {
            iou: {rank: {"True": 0, "Total": 0} for rank in confidence_scores_range}
            for iou in ious_range
        }
        for channel in classes_names
    }
    # nb_gt_objects contains the total number of ground truth objects
    # for each channel in classes_names.
    # Used while computing the recall at object level.
    nb_gt_objects = {channel: 0 for channel in classes_names}

    # tqdm is used to display a progress bar indicating the number of processed elements.
    for file_name in tqdm(os.listdir(labels_dir), desc="Evaluation"):

        # Read the predicted and ground truth json files and retrieve the polygons.
        gt_regions = read_json(os.path.join(labels_dir, file_name))
        pred_regions = read_json(os.path.join(pred_dir, file_name))
        assert gt_regions["img_size"] == pred_regions["img_size"]
        gt_polys = get_polygons(gt_regions, classes_names)
        pred_polys = get_polygons(pred_regions, classes_names)

        # Pixel level
        # Update the pixel metrics dict by appending the current image results: IoU, Precision,
        # Recall and Fscore for each channel.
        pixel_res = p_metrics.update_metrics(
            gt_polys, pred_polys, classes_names, pixel_res
        )

        # Object level
        # Compute the image_rank_scores: number of true positive objects and total number of predicted
        # objects for the current image. One value for each confidence score in range(95, -5, -5),
        # IoU threshold in range(50, 100, 5) and channel in classes_names.
        image_rank_scores = o_metrics.compute_rank_scores(
            ious_range, confidence_scores_range, gt_polys, pred_polys, classes_names
        )
        # Update the rank_scores dict by appending the current image_rank_scores.
        rank_scores = o_metrics.update_rank_scores(
            ious_range,
            confidence_scores_range,
            rank_scores,
            image_rank_scores,
            classes_names,
        )
        # Compute the number of ground truth objects for the current image.
        nb_gt_objects = {
            channel: nb_gt_objects[channel] + len(gt_polys[channel])
            for channel in classes_names
        }

    # Compute the mean Precision, Recall, Fscore, and AP at object level.
    object_res = o_metrics.get_mean_results(
        ious_range,
        confidence_scores_range,
        rank_scores,
        nb_gt_objects,
        classes_names,
        object_res,
        object_metrics,
    )

    # Print and save the results.
    save_results(pixel_res, pixel_metrics, object_res, classes_names)

    end = time.gmtime(time.time() - starting_time)
    logging.info(
        "Finished evaluation in %2d:%2d:%2d", end.tm_hour, end.tm_min, end.tm_sec
    )


def main():
    args = get_cli_args()
    run(args.labels_dir, args.predictions_dir)
