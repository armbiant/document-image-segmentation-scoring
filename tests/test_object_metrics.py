#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from evaluator import evaluate
from evaluator.utils import object_metrics


@pytest.mark.parametrize(
    "labels, predictions, ious_range, confidence_range, classes_names, output",
    [
        (
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
            },
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
            },
            range(50, 100, 25),
            range(100, -5, -50),
            ["text_line"],
            {
                "text_line": {
                    50: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                    75: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                }
            },
        ),
        (
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
                "text_region": [],
            },
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 6], [8, 6], [8, 3], [0, 3]],
                    }
                ],
                "text_region": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 5], [0, 8], [7, 8], [7, 5], [0, 5]],
                    }
                ],
            },
            range(50, 100, 25),
            range(100, -5, -50),
            ["text_line", "text_region"],
            {
                "text_line": {
                    50: {
                        100: {"True": 1, "Total": 1},
                        50: {"True": 1, "Total": 1},
                        0: {"True": 1, "Total": 1},
                    },
                    75: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                },
                "text_region": {
                    50: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                    75: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                },
            },
        ),
    ],
)
def test_compute_rank_scores(
    labels, predictions, ious_range, confidence_range, classes_names, output
):
    labels = evaluate.get_polygons(labels, classes_names)
    predictions = evaluate.get_polygons(predictions, classes_names)
    assert (
        object_metrics.compute_rank_scores(
            ious_range, confidence_range, labels, predictions, classes_names
        )
        == output
    )


@pytest.mark.parametrize(
    "ious_range, confidence_range, global_metrics, current_metrics, classes_names, output",
    [
        (
            range(50, 100, 25),
            range(100, -5, -50),
            {
                "text_line": {
                    50: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                    75: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                }
            },
            {
                "text_line": {
                    50: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                    75: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                }
            },
            ["text_line"],
            {
                "text_line": {
                    50: {
                        100: {"True": 4, "Total": 4},
                        50: {"True": 4, "Total": 4},
                        0: {"True": 4, "Total": 4},
                    },
                    75: {
                        100: {"True": 4, "Total": 4},
                        50: {"True": 4, "Total": 4},
                        0: {"True": 4, "Total": 4},
                    },
                }
            },
        ),
        (
            range(50, 100, 25),
            range(100, -5, -50),
            {
                "text_line": {
                    50: {
                        100: {"True": 1, "Total": 1},
                        50: {"True": 1, "Total": 1},
                        0: {"True": 1, "Total": 1},
                    },
                    75: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                },
                "text_region": {
                    50: {
                        100: {"True": 1, "Total": 1},
                        50: {"True": 1, "Total": 1},
                        0: {"True": 1, "Total": 1},
                    },
                    75: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                },
            },
            {
                "text_line": {
                    50: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                    75: {
                        100: {"True": 2, "Total": 2},
                        50: {"True": 2, "Total": 2},
                        0: {"True": 2, "Total": 2},
                    },
                },
                "text_region": {
                    50: {
                        100: {"True": 0, "Total": 0},
                        50: {"True": 0, "Total": 0},
                        0: {"True": 0, "Total": 0},
                    },
                    75: {
                        100: {"True": 0, "Total": 0},
                        50: {"True": 0, "Total": 0},
                        0: {"True": 0, "Total": 0},
                    },
                },
            },
            ["text_line", "text_region"],
            {
                "text_line": {
                    50: {
                        100: {"True": 3, "Total": 3},
                        50: {"True": 3, "Total": 3},
                        0: {"True": 3, "Total": 3},
                    },
                    75: {
                        100: {"True": 2, "Total": 3},
                        50: {"True": 2, "Total": 3},
                        0: {"True": 2, "Total": 3},
                    },
                },
                "text_region": {
                    50: {
                        100: {"True": 1, "Total": 1},
                        50: {"True": 1, "Total": 1},
                        0: {"True": 1, "Total": 1},
                    },
                    75: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                },
            },
        ),
    ],
)
def test_update_rank_scores(
    ious_range, confidence_range, global_metrics, current_metrics, classes_names, output
):
    assert (
        object_metrics.update_rank_scores(
            ious_range, confidence_range, global_metrics, current_metrics, classes_names
        )
        == output
    )


@pytest.mark.parametrize(
    "ious_range, confidence_range, global_metrics, global_gt, classes_names, results, metrics, output",
    [
        (
            range(50, 100, 25),
            range(100, -5, -50),
            {
                "text_line": {
                    50: {
                        100: {"True": 4, "Total": 4},
                        50: {"True": 4, "Total": 4},
                        0: {"True": 4, "Total": 4},
                    },
                    75: {
                        100: {"True": 4, "Total": 4},
                        50: {"True": 4, "Total": 4},
                        0: {"True": 4, "Total": 4},
                    },
                }
            },
            {"text_line": 12},
            ["text_line"],
            {"text_line": {"precision": [], "recall": [], "fscore": [], "ap": []}},
            ["Precision", "Recall", "Fscore", "AP"],
            {
                "text_line": {
                    "precision": {
                        50: {0: 1.0, 50: 1.0, 100: 1.0},
                        75: {0: 1.0, 50: 1.0, 100: 1.0},
                    },
                    "recall": {
                        50: {0: 1 / 3, 50: 1 / 3, 100: 1 / 3},
                        75: {0: 1 / 3, 50: 1 / 3, 100: 1 / 3},
                    },
                    "fscore": {
                        50: {0: 0.5, 50: 0.5, 100: 0.5},
                        75: {0: 0.5, 50: 0.5, 100: 0.5},
                    },
                    "ap": {50: 1.0, 75: 1.0},
                }
            },
        ),
        (
            range(50, 100, 25),
            range(100, -5, -50),
            {
                "text_line": {
                    50: {
                        100: {"True": 3, "Total": 3},
                        50: {"True": 3, "Total": 3},
                        0: {"True": 3, "Total": 3},
                    },
                    75: {
                        100: {"True": 2, "Total": 3},
                        50: {"True": 2, "Total": 3},
                        0: {"True": 2, "Total": 3},
                    },
                },
                "text_region": {
                    50: {
                        100: {"True": 1, "Total": 1},
                        50: {"True": 1, "Total": 1},
                        0: {"True": 1, "Total": 1},
                    },
                    75: {
                        100: {"True": 0, "Total": 1},
                        50: {"True": 0, "Total": 1},
                        0: {"True": 0, "Total": 1},
                    },
                },
            },
            {"text_line": 12, "text_region": 2},
            ["text_line", "text_region"],
            {
                "text_line": {"precision": [], "recall": [], "fscore": [], "ap": []},
                "text_region": {"precision": [], "recall": [], "fscore": [], "ap": []},
            },
            ["Precision", "Recall", "Fscore", "AP"],
            {
                "text_line": {
                    "precision": {
                        50: {0: 1.0, 50: 1.0, 100: 1.0},
                        75: {
                            0: 0.6666666666666666,
                            50: 0.6666666666666666,
                            100: 0.6666666666666666,
                        },
                    },
                    "recall": {
                        50: {0: 0.25, 50: 0.25, 100: 0.25},
                        75: {
                            0: 0.16666666666666666,
                            50: 0.16666666666666666,
                            100: 0.16666666666666666,
                        },
                    },
                    "fscore": {
                        50: {0: 0.4, 50: 0.4, 100: 0.4},
                        75: {
                            0: 0.26666666666666666,
                            50: 0.26666666666666666,
                            100: 0.26666666666666666,
                        },
                    },
                    "ap": {50: 1.0, 75: 0.6666666666666667},
                },
                "text_region": {
                    "precision": {
                        50: {0: 1.0, 50: 1.0, 100: 1.0},
                        75: {0: 0.0, 50: 0.0, 100: 0.0},
                    },
                    "recall": {
                        50: {0: 0.5, 50: 0.5, 100: 0.5},
                        75: {0: 0.0, 50: 0.0, 100: 0.0},
                    },
                    "fscore": {
                        50: {
                            0: 0.6666666666666666,
                            50: 0.6666666666666666,
                            100: 0.6666666666666666,
                        },
                        75: {0: 0, 50: 0, 100: 0},
                    },
                    "ap": {50: 1.0, 75: 0.0},
                },
            },
        ),
    ],
)
def test_get_mean_results(
    ious_range,
    confidence_range,
    global_metrics,
    global_gt,
    classes_names,
    results,
    metrics,
    output,
):
    assert (
        object_metrics.get_mean_results(
            ious_range,
            confidence_range,
            global_metrics,
            global_gt,
            classes_names,
            results,
            metrics,
        )
        == output
    )
