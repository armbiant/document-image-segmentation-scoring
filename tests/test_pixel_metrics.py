#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from evaluator import evaluate
from evaluator.utils import pixel_metrics


@pytest.mark.parametrize(
    "intersection, label_area, predicted_area, iou",
    [(0, 0, 0, 1), (1, 1, 1, 1), (1, 2, 2, 1 / 3), (1, 0, 0, 1), (0, 1, 1, 0)],
)
def test_get_iou(intersection, label_area, predicted_area, iou):
    assert pixel_metrics.get_iou(intersection, label_area, predicted_area) == iou


@pytest.mark.parametrize(
    "intersection, predicted_area, precision",
    [(0, 0, 1), (1, 1, 1), (1, 2, 0.5), (1, 0, 1), (0, 1, 0)],
)
def test_get_precision(intersection, predicted_area, precision):
    assert pixel_metrics.get_precision(intersection, predicted_area) == precision


@pytest.mark.parametrize(
    "intersection, label_area, recall",
    [(0, 0, 1), (1, 1, 1), (1, 2, 0.5), (1, 0, 1), (0, 1, 0)],
)
def test_get_recall(intersection, label_area, recall):
    assert pixel_metrics.get_recall(intersection, label_area) == recall


@pytest.mark.parametrize(
    "labels, predictions, classes_names, global_metrics, output",
    [
        (
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
            },
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
            },
            ["text_line"],
            {"text_line": {"iou": [], "precision": [], "recall": [], "fscore": []}},
            {
                "text_line": {
                    "iou": [1.0],
                    "precision": [1.0],
                    "recall": [1.0],
                    "fscore": [1.0],
                }
            },
        ),
        (
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
                "text_region": [],
            },
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 2], [0, 6], [7, 6], [7, 2], [0, 2]],
                    }
                ],
                "text_region": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 5], [0, 8], [7, 8], [7, 5], [0, 5]],
                    }
                ],
            },
            ["text_line", "text_region"],
            {
                "text_line": {
                    "iou": [1.0],
                    "precision": [1.0],
                    "recall": [1.0],
                    "fscore": [1.0],
                },
                "text_region": {"iou": [], "precision": [], "recall": [], "fscore": []},
            },
            {
                "text_line": {
                    "iou": [1.0, 0.3684210526315789],
                    "precision": [1.0, 0.5],
                    "recall": [1.0, 0.5833333333333334],
                    "fscore": [1.0, 0.5384615384615384],
                },
                "text_region": {
                    "iou": [0],
                    "precision": [0.0],
                    "recall": [1],
                    "fscore": [0.0],
                },
            },
        ),
    ],
)
def test_update_metrics(labels, predictions, classes_names, global_metrics, output):
    labels = evaluate.get_polygons(labels, classes_names)
    predictions = evaluate.get_polygons(predictions, classes_names)
    assert (
        pixel_metrics.update_metrics(labels, predictions, classes_names, global_metrics)
        == output
    )
