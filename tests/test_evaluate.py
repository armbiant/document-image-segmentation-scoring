#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from evaluator import evaluate


@pytest.mark.parametrize(
    "json_content, polygons, classes_names",
    [
        (
            {
                "img_size": [10, 10],
                "text_line": [
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    },
                    {
                        "confidence": 1.0,
                        "polygon": [[0, 6], [0, 7], [8, 7], [8, 6], [0, 6]],
                    },
                ],
            },
            {
                "text_line": [
                    (1.0, [(0.0, 3.0), (0.0, 5.0), (8.0, 5.0), (8.0, 3.0), (0.0, 3.0)]),
                    (1.0, [(0.0, 6.0), (0.0, 7.0), (8.0, 7.0), (8.0, 6.0), (0.0, 6.0)]),
                ]
            },
            ["text_line"],
        ),
        (
            {
                "img_size": [10, 10],
                "text_line": [],
                "text_region": [
                    {
                        "confidence": 0.5,
                        "polygon": [[0, 3], [0, 5], [8, 5], [8, 3], [0, 3]],
                    }
                ],
            },
            {
                "text_line": [],
                "text_region": [
                    (0.5, [(0.0, 3.0), (0.0, 5.0), (8.0, 5.0), (8.0, 3.0), (0.0, 3.0)])
                ],
            },
            ["text_line", "text_region"],
        ),
    ],
)
def test_get_polygons(json_content, polygons, classes_names):
    retrieved_polygons = evaluate.get_polygons(json_content, classes_names)
    for channel in classes_names:
        for index, polygon in enumerate(retrieved_polygons[channel]):
            retrieved_polygons[channel][index] = (
                polygon[0],
                list(polygon[1].exterior.coords),
            )
    assert retrieved_polygons == polygons
